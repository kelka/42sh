/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sh_parser.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/18 16:42:41 by ccano             #+#    #+#             */
/*   Updated: 2014/06/26 01:11:46 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SH_PARSER_H
# define SH_PARSER_H

# include <sh_lexer.h>
# include <libc.h>


# define P_LEAF			1
# define P_UNARY		2
# define P_BINARY		3

typedef struct s_redi	t_redi;
typedef struct s_redo	t_redo;
typedef struct s_cmd	t_cmd;

struct					s_redo
{
	char				*file;
	int					append;
	t_redo				*next;
	t_redo				*prev;
};

struct					s_redi
{
	char				*file;
	int					append;
	t_redi				*next;
	t_redi				*prev;
};

struct					s_cmd
{
	char				*filename;
	t_redi				*s_redi;
	t_redo				*s_redo;
	char				**argv;
	int					piped;
	t_cmd				*next;
	t_cmd				*prev;
	int					perm;
};

typedef struct			s_ast
{
	t_cmd				*s_cmd;
	int					bkg;
	struct s_ast		*next;
	struct s_ast		*prev;
}						t_ast;

typedef struct			s_parser
{
	t_lexem				*s_blexem;
	t_lexem				*s_lexem;
	t_ast				*s_ast;
}						t_parser;

t_ast					*ast_new_pushback(t_ast	**s_bast);
int						build_cmd(t_parser *s_parser);
int						build_redi(t_parser *s_parser);
int						build_redo(t_parser *s_parser);

int						sh_parser(t_lexem *s_blexem, t_parser **s_parser);
int						parse_init(t_parser *s_parser);
int						parse_ril(t_parser *s_parser);
int						parse_rir(t_parser *s_parser);
int						parse_cmd(t_parser *s_parser);
int						parse_ro(t_parser *s_parser);
int						parse_pipe(t_parser *s_parser);
int						parse_eof(t_parser *s_parser);
int						parse_sep(t_parser *s_parser);
int						parse_bkg(t_parser *s_parser);
void					parse_fatal(t_parser *s_parser);

#endif
