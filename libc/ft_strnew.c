/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/24 04:35:52 by ccano             #+#    #+#             */
/*   Updated: 2014/03/27 21:41:46 by scarre           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libc.h>

char		*ft_strnew(size_t size)
{
	char	*buf;

	if (size < 1)
		return (NULL);
	else if ((buf = ft_memalloc(size + 1)) == NULL)
		return (NULL);
	return (buf);
}
