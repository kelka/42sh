/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/24 04:23:39 by ccano             #+#    #+#             */
/*   Updated: 2014/03/27 21:39:21 by scarre           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libc.h>

void		*ft_memalloc(size_t size)
{
	void		*buf;

	if (size < 1)
		return (NULL);
	if ((buf = malloc(size)) == NULL)
		return (NULL);
	ft_bzero(buf, size);
	return (buf);
}
