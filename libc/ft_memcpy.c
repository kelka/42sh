/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/24 02:05:53 by ccano             #+#    #+#             */
/*   Updated: 2014/03/27 21:39:33 by scarre           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libc.h>

void		*ft_memcpy(void *dest, const void *src, size_t size)
{
	unsigned char	*dest_ptr;

	dest_ptr = (unsigned char *)dest;
	if (dest)
	{
		while (size--)
			*dest_ptr++ = *(unsigned char *)src++;
	}
	return ((void *)dest);
}
