/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsarfati <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/19 19:36:59 by gsarfati          #+#    #+#             */
/*   Updated: 2014/03/27 21:44:06 by scarre           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libc.h>

char		*ft_strtrim(char const *s)
{
	int		count[5] = {0, ft_strlen(s), 0, -1, -1};
	char	*new;

	while (s[count[0]])
	{
		if (s[count[0]] > 32)
			count[3] = (count[3] == -1) ? count[0] : count[3];
		if (s[count[1]] > 32)
			count[4] = (count[4] == -1) ? count[1] : count[4];
		count[0]++;
		count[1]--;
	}
	new = malloc(sizeof(*new) * (count[4] - count[3] + 2));
	while (s[count[3]])
	{
		new[count[2]++] = s[count[3]++];
	}
	new[count[2]] = '\0';
	return (new);
}
