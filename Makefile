# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lverniss <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/03/13 16:51:09 by lverniss          #+#    #+#              #
#    Updated: 2014/06/26 10:22:04 by ccano            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	=	42sh

SRCS	=	./src/main.c ./src/ft_prompt.c ./libc/ft_strlen.c \
			./libc/ft_error.c ./src/ft_shrc.c ./libc/ft_strtrim.c \
			./libc/ft_strdup.c ./libc/ft_putstr.c ./src/ft_cpyenv.c \
			./libc/ft_strncmp.c ./src/ft_getenv.c ./libc/ft_bzero.c \
			./src/ft_read.c ./libc/ft_strsplit.c ./libc/ft_strcat.c \
			./src/ft_mapexe.c ./libc/ft_strcpy.c ./src/ft_pathuser.c \
			./src/ft_freeread.c ./libc/ft_strnew.c ./libc/ft_memcpy.c \
			./libc/ft_memalloc.c ./libc/ft_isalnum.c ./libc/ft_putchar.c \
			./libc/ft_putstr_fd.c ./libc/ft_isalpha.c ./libc/ft_isdigit.c \
			./src/lexer/fsm.c ./src/lexer/fsm_ft.c ./src/lexer/lexem_list.c ./src/lexer/sh_lexer.c \
			./src/lexer/ft_ast.c  \
			./src/parser/sh_parser.c \
			./src/parser/parse_bkg.c ./src/parser/parse_pipe.c\
			./src/parser/parse_ro.c ./src/parser/parse_ri.c ./src/parser/parse_cmd.c \
			./src/parser/parse_sep.c ./src/parser/parse_fatal.c ./src/parser/parse_eof.c\
			./src/parser/parse_init.c \
			./src/exec/sh_exec_cmd.c ./src/exec/sh_pipe.c ./src/exec/sh_proc.c \
			./src/exec/sh_exec_red.c ./src/exec/sh_exec_builtin.c ./src/exec/get_cmd.c\
			./src/get_pexe.c \
			./src/ft_ptrenv.c ./src/builtin.c ./src/init_buil_ptr.c\
			./src/ft_addmapelem.c ./src/ft_addexe.c ./src/ft_addbuiltin.c \
			./libc/samestr.c
# ./libc/ft_memchr.c\



SRC	= $(addprefix $(SRCPATH), $(SRCS))

OBJ	= $(SRC:.c=.o)

TEST	= ./src/main.o

CFLAGS	+=  -Wall -Wextra -I ./includes -ggdb3 -lcurses

all: $(NAME)

$(NAME): $(OBJ)
	@cc $(CFLAGS) -o $(NAME) $(SRC)
	@echo "\033[1;32m[ $(NAME) Created ]\033[1;32m"

clean :
	@if test -f $(TEST) ;then \
		rm -f $(OBJ) && echo "\033[1;32m[ OBJCLEAN : DONE ]\033[0;00m"; \
		else echo "\033[1;31m[ OBJCLEAN : file not exist ]\033[0;00m"; \
		fi

fclean : clean
	@if test -e $(NAME) ;then \
		rm -f $(NAME) && echo "\033[1;32m[ BINCLEAN : DONE ]\033[0;00m"; \
		else echo "\033[1;31m[ BINCLEAN : file not exist ]\033[0;00m"; \
		fi

re: fclean all

.PHONY: all clean fclean re
