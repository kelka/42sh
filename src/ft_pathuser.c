/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pathuser.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scarre <scarre@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/27 22:15:28 by scarre            #+#    #+#             */
/*   Updated: 2014/03/27 22:15:41 by scarre           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh.h>

t_sh	*pathuser(t_sh *sh)
{
	char	*new;

	if (!(new = malloc(sizeof(*new) * (ft_strlen(sh->user->home) + \
						(ft_strlen(FILEHIST) + 2)))))
		return (0);
	new = ft_strcpy(new, sh->user->home);
	new = ft_strcat(new, "/");
	new = ft_strcat(new, FILECONF);
	sh->user->pshrc = ft_strdup(new);
	free(new);
	return (sh);
}
