/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sh_exec_builtin.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/24 04:14:48 by ccano             #+#    #+#             */
/*   Updated: 2014/06/24 04:27:42 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <sh.h>

int						sh_exec_builtin(t_cmd *s_cmd, t_sh *s_sh)
{
	static build		*built_ftptr = NULL;

#if __DEBUG_EXEC__
	printf("exec_builtins\n");
#endif
	if (!built_ftptr)
		built_ftptr = init_buil_ptr();
	return (built_ftptr[s_cmd->perm - 2](s_sh, s_cmd->argv));
}
