/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sh_pipe.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scarre <scarre@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/27 23:17:58 by scarre            #+#    #+#             */
/*   Updated: 2014/06/26 05:33:20 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh.h>

static int	exec_child(int *fd, t_exe_fd *s_exe, t_sh *sh, t_cmd *s_cmd)
{
	int		ret;

	ret = 1;
	setpgid(getpid(), sh->pgid);
	close(fd[0]);
	dup2(fd[1], 1);
	s_exe->fd_out = fd[1];
	if (s_cmd->s_redi)
		ret = sh_exec_redi(s_cmd, s_exe);
	if (s_cmd->s_redo && (ret > 0))
		ret = sh_exec_redo(s_cmd, s_exe);
	if (ret > 0)
		ret = sh_exec_cmd(s_cmd, sh, s_exe);
	exit(-1);
}

int			sh_exec_pipe(t_cmd *s_cmd, t_exe_fd *s_exe, t_sh *sh, int bkg)
{
	int		fd[2];
	int		ret;
	int		child;

	if (!(ret = pipe(fd)))
	{
		ret = 1;
		if ((child = fork()) > -1)
		{
			if (child)
				exec_child(fd, s_exe, sh, s_cmd);
			else if (!(ret = 0))
			{
				close(fd[1]);
				dup2(fd[0], 0);
				s_exe->fd_in = fd[0];
				s_cmd = s_cmd->next;
				sh_exec(s_cmd, s_exe, sh, bkg);
				exit(-1);
			}
		}
		else
			ret = -2;
	}
	exit (ret);
}
