/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sh_proc 2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/25 23:59:23 by ccano             #+#    #+#             */
/*   Updated: 2014/06/26 06:27:21 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <sh.h>

static int		set_proc(t_sh *s_sh)
{
	int			pid;

	signal(SIGINT, SIG_DFL);
	signal(SIGQUIT, SIG_DFL);
	signal(SIGTSTP, SIG_DFL);
	signal(SIGTTIN, SIG_DFL);
	signal(SIGTTOU, SIG_DFL);
	signal(SIGCHLD, SIG_DFL);
	pid = getpid();
	if (setpgid(pid, s_sh->pgid))
		return (-1);
	return (0);
}

static int		do_op(t_cmd *s_cmd, t_exe_fd *s_exe, t_sh *s_sh, int bkg)
{
	int			ret;

	ret = 1;
	if (!set_proc(s_sh))
	{
		if (s_cmd->piped)
			sh_exec_pipe(s_cmd, s_exe, s_sh, bkg);
		else
		{
			if (s_cmd->s_redi)
				ret = sh_exec_redi(s_cmd, s_exe);
			if ((ret > 0) && s_cmd->s_redo)
				ret = sh_exec_redo(s_cmd, s_exe);
			if (ret > 0)
				sh_exec_cmd(s_cmd, s_sh, s_exe);
		}
	}
	if (IS_BUILTIN(s_cmd->perm))
		return (ret);
		_exit(EXIT_FAILURE);
}

static int		to_fg(pid_t pid)
{
	int			status;

	waitpid(pid, &status, WUNTRACED);
	printf("hey ret: %d\n", status);
	return (status);
}

int				sh_exec(t_cmd *s_cmd, t_exe_fd *s_exe, t_sh *s_sh, int bkg)
{
	pid_t		pid;
	int			status;

	status = EXIT_FAILURE;
	if (get_filename(s_cmd, s_sh))
	{
		if (IS_BUILTIN(s_cmd->perm))
			status = do_op(s_cmd, s_exe, s_sh, bkg);
		else if ((pid = fork()) < 0)
			return (ERR_FORK);
		else if (!pid)
			status = do_op(s_cmd, s_exe, s_sh, bkg);
		else if (!bkg)
			status = to_fg(pid);
		//	tcsetpgrp(STDIN, s_sh->pgid);
		//	tcsetattr(STDIN, TCSADRAIN, s_sh->term_save);
	}
	else
		printf("%s: command not found.\n", s_cmd->argv[0]);
	return (status);
}
