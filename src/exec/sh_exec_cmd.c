/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sh_exec.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/24 03:41:10 by ccano             #+#    #+#             */
/*   Updated: 2014/06/26 06:27:51 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh.h>
#include <stdio.h>

int			sh_exec_cmd(t_cmd *cmd, t_sh un *sh, t_exe_fd un *s_exe)
{
	if (IS_BUILTIN(cmd->perm))
		return (sh_exec_builtin(cmd, sh));
	if ((access(cmd->filename, F_OK)) || (access(cmd->filename, X_OK)))
	{
		printf("ici\n");
		if (access(cmd->filename, F_OK))
			printf("%s: command not found.\n", cmd->filename);
		else
			printf("%s: permission denied.\n", cmd->filename);
		_exit(-3);
	}
	execv(cmd->filename, cmd->argv);
	printf("execv failed\n");
	_exit(EXIT_FAILURE);
}
