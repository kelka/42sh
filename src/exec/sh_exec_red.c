/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sh_exec_red.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/24 03:55:33 by ccano             #+#    #+#             */
/*   Updated: 2014/06/24 04:29:38 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh.h>

int			sh_exec_redi(t_cmd *cmd, t_exe_fd *s_exe)
{
	int		fd_in;
	int		ret;

	ret = 0;
	fd_in = -1;
	while (cmd->s_redi)
	{
		if (fd_in > -1)
			close(fd_in);
		if (!(access(cmd->s_redi->file, F_OK))
				&& !(access(cmd->s_redi->file, R_OK)))
			fd_in = open(cmd->s_redi->file, O_RDONLY, 0744);
		else
		{
			printf("Cannot access %s: no such file or directory.\n", cmd->s_redi->file);
			return (-3);
		}
		if ((fd_in > -1) && (ret = 1))
			dup2(fd_in, 0);
		else
			ret = -3;
		s_exe->fd_in = fd_in;
		cmd->s_redi = cmd->s_redi->next;
	}
	return (ret);
}

int			sh_exec_redo(t_cmd *cmd, t_exe_fd *s_exe)
{
	int		fd_out;
	int		ret;

	ret = 0;
	fd_out = -1;
	while (cmd->s_redo)
	{

		if (fd_out > -1)
			close(fd_out);
		if (cmd->s_redo->append)
		{
			fd_out = open(cmd->s_redo->file,
					O_CREAT | O_APPEND | O_WRONLY, 0744);
		}
		else
			fd_out = open(cmd->s_redo->file, O_CREAT | O_TRUNC | O_RDWR, 0744);
		if ((fd_out > -1) && (ret = 1))
			dup2(fd_out,1);
		else
			ret = -3;
		s_exe->fd_out = fd_out;
		cmd->s_redo = cmd->s_redo->next;
	}
	return (ret);
}
