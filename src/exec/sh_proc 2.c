/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sh_proc 2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/25 23:59:23 by ccano             #+#    #+#             */
/*   Updated: 2014/06/26 00:06:03 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <sh.h>

static int		set_proc(t_sh *s_sh)
{
	int			pid;

	signal(SIGINT, SIG_DFL);
	signal(SIGQUIT, SIG_DFL);
	signal(SIGTSTP, SIG_DFL);
	signal(SIGTTIN, SIG_DFL);
	signal(SIGTTOU, SIG_DFL);
	signal(SIGCHLD, SIG_DFL);
	pid = getpid();
	if (setpgid(pid, s_sh->pgid))
		return (-1);
	return (0);
}

static int		do_op(t_cmd *s_cmd, t_exe_fd *s_exe, t_sh *sh, int bkg)
{
	int			ret;

	ret = 1;
	if (!set_proc(sh))
	{
		if (s_cmd->piped)
			sh_exec_pipe(s_cmd, s_exe, s_sh, bkg);
		else
		{
			if (s_cmd->s_redi)
				ret = sh_exec_redi(s_cmd, s_exe);
			if ((ret > 0) && s_cmd->s_redo)
				ret = sh_exec_redo(s_cmd, s_exe);
			if (ret > 0)
				sh_exec_cmd(s_cmd, s_sh, s_exe);
		}
	}
	_exit(EXIT_FAILURE);
}

static int		set_proc(t_sh *s_sh)
{
	signal(SIGINT, SIG_DFL);
	signal(SIGQUIT, SIG_DFL);
	signal(SIGTSTP, SIG_DFL);
	signal(SIGTTIN, SIG_DFL);
	signal(SIGTTOU, SIG_DFL);
	signal(SIGCHLD, SIG_DFL);
	pid = getpid();
	if (setpgid(pid, s_sh->pgid))
		return (-1);
	return (0);
}

static int		to_fg(pid_t pid)
{
	int			status;

	waitpid(pid, &status, WUNTRACED);
	return (status);
}

int				sh_exec(t_cmd *s_cmd, t_exe_fd *s_exe, t_sh *s_sh, int bkg)
{
	pid_t		pid;

	if (!bkg)
	{
		if ((pid = vfork()) < 0)
			return (ERR_FORK);
	}
	else if ((s_proc->pid = fork()) < 0)
			return (ERR_FORK);
	if (!s_proc->pid)
		do_op(s_cmd, s_exe, sh, bkg);
	if (!bkg)
		status = to_fg(pid);
	tcsetpgrp(STDIN, s_sh->pgid);
	tcsetattr(STDIN, TCSADRAIN, s_sh->term_save);
	return (status);
}
