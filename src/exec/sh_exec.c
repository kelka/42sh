
#include <sh.h>
#include <stdio.h>

int			sh_exec_redi(t_cmd *cmd, t_exe_fd *s_exe)
{
   int		fd_in;
   int		ret;

   ret = 0;
   fd_in = -1;
   if (cmd->s_redi)
   {
	  if (!(access(cmd->s_redi->file, F_OK)) && !(access(cmd->s_redi->file, R_OK)))
		 fd_in = open(cmd->s_redi->file, O_RDONLY, 0744);
	  else
	  {
		 printf("Cannot access %s: no such file or directory.\n", cmd->s_redi->file);
		 return (-3);
	  }
	  if ((fd_in > -1) && (ret = 1))
		 dup2(fd_in, 0);
	  else
		 ret = -3;
	  s_exe->fd_in = fd_in;
	  //cmd->s_redi = cmd->s_redi->next;
   }
   return (ret);
}

int			sh_exec_redo(t_cmd *cmd, t_exe_fd *s_exe)
{
   int		fd_out;
   int		ret;

   ret = 0;
   if (cmd->s_redo)
   {
	  if (cmd->s_redo->append)
		 fd_out = open(cmd->s_redo->file, O_CREAT | O_APPEND | O_WRONLY, 0744);
	  else
		 fd_out = open(cmd->s_redo->file, O_CREAT | O_TRUNC | O_RDWR, 0744);
	  if ((fd_out > -1) && (ret = 1))
		 dup2(fd_out,1);
	  else
		 ret = -3;
	  s_exe->fd_out = fd_out;
	  //cmd->s_redo = cmd->s_redo->next;
   }
   return (ret);
}

static int	get_filename(t_cmd *s_cmd, t_sh *sh)
{
   if (s_cmd->argv[0] && (*s_cmd->argv[0] == '/'))   
   {
	  s_cmd->filename = s_cmd->argv[0];
	  return (1);
   }
   else if (s_cmd->argv[0] && (*s_cmd->argv[0] == '.') && (*(s_cmd->argv[0] + 1) == '/'))
   {
	  s_cmd->filename = s_cmd->argv[0] + 2;
	  return (1);
   }
   else if ((get_pexe(sh, s_cmd)))
	  return (1);
   return (0);
}

static int			exec_builtin(t_cmd *s_cmd, t_sh *s_sh)
{
   static build		*built_ftptr = NULL;
	  

#if __DEBUG_EXEC__
   printf("exec_builtins\n");
#endif
   if (!built_ftptr)
	  built_ftptr = init_buil_ptr();
   return (built_ftptr[s_cmd->perm - 2](s_sh, s_cmd->argv));
}

int			sh_exec_cmd(t_cmd *cmd, t_sh un *sh, t_exe_fd un *s_exe)
{
  if (get_filename(cmd, sh))
   {
	  if (IS_BUILTIN(cmd->perm))
		 return (exec_builtin(cmd, sh));
	  if ((access(cmd->filename, F_OK)) || (access(cmd->filename, X_OK)))
	  {
		 if (access(cmd->filename, F_OK))
			printf("%s: command not found.\n", cmd->filename);
		 else
			printf("%s: permission denied.\n", cmd->filename);
		 _exit(-3);
	  }
	  exit (execv(cmd->filename, cmd->argv));
	  printf("execv failed\n");
	  _exit(EXIT_FAILURE);
   }
   else
   {
	  printf("%s: command not found.\n", cmd->argv[0]);
	  _exit(EXIT_FAILURE);
   }
}

