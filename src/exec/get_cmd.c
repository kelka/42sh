#include <sh.h>

static int		init_t_exe_fd(t_exe_fd **s_exe)
{  
	if (!(*s_exe) && !(*s_exe = (t_exe_fd *)malloc(sizeof(**s_exe))))
		return (ERR_MALLOC);
	(*s_exe)->in_bk = dup(fileno(stdin));
	(*s_exe)->out_bk = dup(fileno(stdout));
	(*s_exe)->fd_in = (*s_exe)->in_bk;
	(*s_exe)->fd_out = (*s_exe)->out_bk;
	return (0);
}

static int		exe_cmd(t_exe_fd **s_exe, t_sh *sh, t_ast *ast)
{
	int			ret;

	ret = 1;
	if (ast->s_cmd)
	{
		if (init_t_exe_fd(s_exe) == ERR_MALLOC)
			return (ERR_MALLOC);
		if (ast->bkg)
			ret = sh_exec(ast->s_cmd, *s_exe, sh, 1);
		else
			ret = sh_exec(ast->s_cmd, *s_exe, sh, 0);
	}
	return (ret);
}

static void			free_cmd(t_cmd *cmd)
{
	t_redi			*s_redi;
	t_redo			*s_redo;

	if (cmd)
	{
		while (cmd->s_redi)
		{
			s_redi = cmd->s_redi;
			cmd->s_redi = cmd->s_redi->next;
			free(s_redi);
		}
		while (cmd->s_redo)
		{
			s_redo = cmd->s_redo;
			cmd->s_redo = cmd->s_redo->next;
			free(s_redo);
		}
//		free(cmd->filename);
		free(cmd->argv);
		free(cmd);
	}
}

void				free_ast(t_ast **ast)
{
	t_ast			*tmp;

	while (*ast)
	{
		tmp = *ast;
		*ast = (*ast)->next;
		free(tmp);
	}
	*ast = NULL;
}

void				free_lexem(t_lexem **lexem)
{
	t_lexem			*tmp;

	while (*lexem)
	{
		tmp = *lexem;
		*lexem = (*lexem)->next;
		free(tmp->lexem);
		free(tmp);
	}
	*lexem = NULL;
}

int					get_cmd(char *line, t_sh *sh)
{
	t_lexem			*lexem;
	t_parser		*parser;
	t_ast			*ast;
	static t_exe_fd	*s_exe = NULL;
	int				ret;

	if ((ret = sh_lexer(line, &lexem)) == EXIT_SUCCESS)
	{
		if ((ret = sh_parser(lexem, &parser)) > -1)
		{
				ast = parser->s_ast;
				while (ast)
				{
					if (ast)
						ret = exe_cmd(&s_exe, sh, ast);
					printf("ret: %d\n", WEXITSTATUS(ret));
					free_cmd(ast->s_cmd);
					ast = ast->next;
				}
				free_ast(&parser->s_ast);
				free_lexem(&lexem);
				ret = 1;
		}
		else
			ret = 1;
	}
	return (ret);
}
