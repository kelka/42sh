/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lverniss <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/25 00:35:06 by lverniss          #+#    #+#             */
/*   Updated: 2014/03/27 23:35:17 by scarre           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <sh.h>

int				get_cmd(char *line, t_sh *sh)
{
   t_lexem		*s_lexem;
   t_parser		*s_parser;
   t_ast		*s_ast;
   int			ret;

   if ((ret = sh_lexer(line, &s_lexem) > 0))
		 ret = sh_parser(s_lexem, &s_parser);
   if (ret >= 0)
   {
	  s_ast = s_parser->s_ast;
	  while (s_ast)
	  {
		 sh_exec(s_ast->s_cmd, sh);
		 s_ast = s_ast->next;
	  }
	  return (1);
   }
   else
	  return (ret);
}


char	core(t_sh *sh)
{
	char	*new;
	int		ret;

	aff_prompt(sh);
	if (!(new = ft_readcmd()))
	{
		free(new);
		return (1);
	}
	ret = get_cmd(new, sh);
	free(new);
	return (ret);
}


t_sh	*loadshell(t_sh *sh, char **envi)
{
	if (!(sh = malloc(sizeof(*sh))))
		return (0);
	if (!(sh->env = ft_cpyenv(envi)))
		return (0);
	if (!(sh->user = malloc(sizeof(*sh->user))))
		return (0);
	sh->user->user = ft_getenv(sh->env, "USER=");
	sh->user->pwd = ft_getenv(sh->env, "PWD="); /* le chargement de l'environnement */
	sh->user->path = ft_getenv(sh->env, "PATH=");
	sh->user->term = ft_getenv(sh->env, "TERM=");
	sh->user->oldpwd = ft_getenv(sh->env, "OLDPWD=");
	sh->user->home = ft_getenv(sh->env, "HOME=");
	sh->mapexe = ft_mapexe(sh);
	sh = pathuser(sh);
	ft_shrc(sh);
	sh->ae = 1;
	return (sh);
}

int	main(int un ac, char un **av, char un **envi)
{
	t_sh	*sh;

	sh = NULL;
	if (!(sh = loadshell(sh, envi)))
		sh->ae = 0;
	while ((core(sh)) > 0)
		;
	return (0);
}
