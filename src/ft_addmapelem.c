/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_addmapelem.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scarre <scarre@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/27 22:12:21 by scarre            #+#    #+#             */
/*   Updated: 2014/03/27 22:12:47 by scarre           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh.h>

t_exe	*init_exe(t_exe *in)
{
	if (!(in = malloc(sizeof(*in))))
		return (0);
	in->next = malloc(sizeof(*in->next) * '~');
	in->pexe = NULL;
	in->ac = 0;
	ft_bzero(in->next, sizeof(*in->next));
	return (in);
}
