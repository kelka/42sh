#include <sh.h>

t_sh	*changepwd(t_sh un *sh)
{
	  char			*buff;
	  const char	*p = "PWD=";
	  char			*buf;
	  char			*tmp;
	  char			*tmp2;
	  t_env			*pwd;
	  t_env			*old;

	  buff = NULL;
	  buff = getcwd(buff, 0);
	  printf("%s\n", buff);
	  if (!(tmp = malloc(sizeof(*tmp) * (ft_strlen(p) + ft_strlen(buff) + 1))))
		 return (0);
	  buf = ft_getenv(sh->env, "PWD=");
	  if (!(tmp2 = malloc(sizeof(*tmp) * (ft_strlen("OLDPWD=") + ft_strlen(buf) + 1))))
		 return (0);
	  tmp2 = ft_strcpy(tmp2, "OLDPWD=");
	  tmp2 = ft_strcat(tmp2, buf);
	  tmp = ft_strcpy(tmp, (char *)p);
	  tmp = ft_strcat(tmp, buff);
	  pwd = r_env_ptr(sh->env, (char *)p);
	  old = r_env_ptr(sh->env, "OLDPWD=");
	  ft_bzero(old->line, ft_strlen(old->line));
	  free(old->line);
	  old->line = ft_strdup(tmp2);
	  free(tmp2);
	  ft_bzero(pwd->line, ft_strlen(pwd->line));
	  free(pwd->line);
	  pwd->line = ft_strdup(tmp);
	  free(tmp);
	  return (sh);
}

char	s_exit(t_sh *sh, char un **arg)
{
   sh->exit = 1;
   exit (0);
}

char	s_env(t_sh *sh, char un **arg)
{
   t_env	*tmp;

   tmp = sh->env;
   while (tmp && tmp->next)
   {
	  printf("%s\n", tmp->line);
	  tmp = tmp->next;
   }
   return (1);
}

char	s_cd(t_sh *sh, char **arg)
{
#if	__DEBUG_EXEC__
   printf("exec s_cd()\n");
#endif
   if (!(access(arg[1], F_OK)))
   {
	  chdir(arg[1]);
	  sh = changepwd(sh);
   }
   else if (arg[1] == NULL)
   {
	  chdir(sh->user->home);
	  sh = changepwd(sh);
   }
   else if (arg[1][0] == '~' && arg[1][1] == '\0')
   {
	  chdir(sh->user->home);
	  sh = changepwd(sh);
   }
   else if (arg[1][0] == '-' && arg[1][1] == '\0')
   {
	  chdir(sh->user->oldpwd);
	  sh = changepwd(sh);
   }
   else
	  ft_error("Access Fail.\n");
   return (1);
}
