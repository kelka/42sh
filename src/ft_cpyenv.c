/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cpyenv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scarre <scarre@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/27 22:13:04 by scarre            #+#    #+#             */
/*   Updated: 2014/03/27 22:13:05 by scarre           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh.h>

t_env	*ft_cpyenv(char **env)
{
	size_t	cn;
	t_env	*start;
	t_env	*tmp;

	if (env[0] == '\0')
		return (0);
	if (!(tmp = malloc(sizeof(*tmp))))
		return (0);
	cn = -1;
	start = tmp;
	while (env[++cn])
	{
		if (env[cn] != '\0')
		{
			tmp->line = ft_strdup(env[cn]);
			if (!(tmp->next = malloc(sizeof(*tmp->next))))
				return (0);
			tmp = tmp->next;
		}
	}
	tmp->next = NULL;
	return (start);
}
