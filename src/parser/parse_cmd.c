/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_cmd.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/24 02:13:58 by ccano             #+#    #+#             */
/*   Updated: 2014/06/26 09:51:45 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh.h>

static int	expect(t_parser *s_parser)
{
	if (s_parser->s_lexem && (s_parser->s_lexem->type == S_REDO))
		return (parse_ro(s_parser));
	if (s_parser->s_lexem && (s_parser->s_lexem->type == S_REDI))
		return (parse_ril(s_parser));
	if (s_parser->s_lexem && (s_parser->s_lexem->type == S_SEP))
		return (parse_sep(s_parser));
	if (s_parser->s_lexem && (s_parser->s_lexem->type == S_PIPE))
		return (parse_pipe(s_parser));
	if (s_parser->s_lexem && (s_parser->s_lexem->type == S_BKG))
		return (parse_bkg(s_parser));
	if (!s_parser->s_lexem)
		return (parse_eof(s_parser));
	parse_fatal(s_parser);
	return (ERR_PARSING);
}

int			build_cmd(t_parser *s_parser)
{
	if (!(s_parser->s_ast->s_cmd) || s_parser->s_ast->s_cmd->argv)
	{
		if (s_parser->s_ast->s_cmd && s_parser->s_ast->s_cmd->argv)
		{
			s_parser->s_ast->s_cmd->next
				= malloc(sizeof(*s_parser->s_ast->s_cmd));
			s_parser->s_ast->s_cmd->next->prev = s_parser->s_ast->s_cmd;
			s_parser->s_ast->s_cmd = s_parser->s_ast->s_cmd->next;
		}
		else if (!s_parser->s_ast->s_cmd)
		{
			if (!(s_parser->s_ast->s_cmd = \
						malloc(sizeof(*s_parser->s_ast->s_cmd))))
				return (0);
			s_parser->s_ast->s_cmd->prev = NULL;
		}
		s_parser->s_ast->s_cmd->s_redi = NULL;
		s_parser->s_ast->s_cmd->s_redo = NULL;
		s_parser->s_ast->s_cmd->argv = NULL;
		s_parser->s_ast->s_cmd->next = NULL;
		s_parser->s_ast->s_cmd->piped = 0;
	}
	return (1);
}

int			parse_cmd(t_parser *s_parser)
{
	int		n;

	n = 0;
	if (!(build_cmd(s_parser)))
		return (ERR_MALLOC);
	// DIRTY MAALLOC
	if (!(s_parser->s_ast->s_cmd->argv =
				ft_memalloc(sizeof(*s_parser->s_ast->s_cmd->argv) * 100)))
		return (ERR_MALLOC);
	while (s_parser->s_lexem && (s_parser->s_lexem->type == S_ALPHA))
	{
		s_parser->s_ast->s_cmd->argv[n++] = s_parser->s_lexem->lexem;
		s_parser->s_lexem = s_parser->s_lexem->next;
	}
	return (expect(s_parser));
}
