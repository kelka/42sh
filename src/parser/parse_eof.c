/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_eof.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/24 02:33:02 by ccano             #+#    #+#             */
/*   Updated: 2014/06/26 09:49:09 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh.h>

static void	reverse_cmd_prop(t_cmd *s_cmd)
{
	while (s_cmd)
	{
		if (s_cmd->s_redi)
		{
			while (s_cmd->s_redi->prev)
				s_cmd->s_redi = s_cmd->s_redi->prev;
		}
		if (s_cmd->s_redo)
		{
			while (s_cmd->s_redo->prev)
				s_cmd->s_redo = s_cmd->s_redo->prev;
		}
		s_cmd = s_cmd->prev;
	}
}

static void	reverse_cmd(t_ast *s_ast)
{
	while (s_ast->s_cmd->prev)
	{
		reverse_cmd_prop(s_ast->s_cmd);
		s_ast->s_cmd = s_ast->s_cmd->prev;
	}
	reverse_cmd_prop(s_ast->s_cmd);
}

static void reverse_ast(t_parser *s_parser)
{
	while (s_parser->s_ast->prev)
	{
		if (s_parser->s_ast->s_cmd)
			reverse_cmd(s_parser->s_ast);
		s_parser->s_ast = s_parser->s_ast->prev;
	}
	if (s_parser->s_ast->s_cmd)
		reverse_cmd(s_parser->s_ast);
}

int			parse_eof(t_parser *s_parser)
{
	if (s_parser->s_ast)
		reverse_ast(s_parser);
	return (0);
}
