/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_bkg.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/24 01:49:32 by ccano             #+#    #+#             */
/*   Updated: 2014/06/24 01:59:23 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh.h>

static int		expect(t_parser *s_parser)
{
	if (!s_parser->s_lexem)
		return (parse_eof(s_parser));
	if (s_parser->s_lexem->type == S_PIPE)
		return (parse_pipe(s_parser));
	if (s_parser->s_lexem->type == S_SEP)
		return (parse_sep(s_parser));
	parse_fatal(s_parser);
	return (ERR_PARSING);
}

int				parse_bkg(t_parser *s_parser)
{
	if (ft_strlen(s_parser->s_lexem->lexem) == 1)
	{
		s_parser->s_ast->bkg = 1;
		s_parser->s_lexem = s_parser->s_lexem->next;
		return (expect(s_parser));
	}
	parse_fatal(s_parser);
	return (ERR_PARSING);
}
