/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_ro.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/24 02:09:12 by ccano             #+#    #+#             */
/*   Updated: 2014/06/26 10:09:32 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh.h>

static int		expect(t_parser *s_parser)
{
	if (!s_parser->s_lexem)
		return (parse_eof(s_parser));
	if (s_parser->s_lexem->type == S_SEP)
		return (parse_sep(s_parser));
	if (s_parser->s_lexem->type == S_REDO)
		return (parse_ro(s_parser));
	parse_fatal(s_parser);
	return (ERR_PARSING);
}

int				build_redo(t_parser *s_parser)
{
	if (s_parser->s_ast->s_cmd->s_redo)
	{
		if (!(s_parser->s_ast->s_cmd->s_redo->next =
					malloc(sizeof(*s_parser->s_ast->s_cmd->s_redo))))
			return (0);
		s_parser->s_ast->s_cmd->s_redo->next->prev =
			s_parser->s_ast->s_cmd->s_redo;
		s_parser->s_ast->s_cmd->s_redo =
			s_parser->s_ast->s_cmd->s_redo->next;
	}
	else if (!(s_parser->s_ast->s_cmd->s_redo =
				malloc(sizeof(*s_parser->s_ast->s_cmd->s_redo)))
			|| (s_parser->s_ast->s_cmd->s_redo
				&& (s_parser->s_ast->s_cmd->s_redo->prev = NULL)))
		return (0);
	s_parser->s_ast->s_cmd->s_redo->file = NULL;
	s_parser->s_ast->s_cmd->s_redo->next = NULL;
	return (1);
}

int				parse_ro(t_parser *s_parser)
{
	s_parser->s_lexem = s_parser->s_lexem->next;
	if (s_parser->s_lexem && (ft_strlen(s_parser->s_lexem->prev->lexem) < 3))
	{
		if (!(build_redo(s_parser)))
			return (ERR_PARSING);
		s_parser->s_ast->s_cmd->s_redo->file = s_parser->s_lexem->lexem;
		if (*(s_parser->s_lexem->prev->lexem + 1) == '>')
			s_parser->s_ast->s_cmd->s_redo->append = 1;
		else
			s_parser->s_ast->s_cmd->s_redo->append = 0;
		s_parser->s_lexem = s_parser->s_lexem->next;
		return (expect(s_parser));
	}
	parse_fatal(s_parser);
	return (ERR_PARSING);
}
