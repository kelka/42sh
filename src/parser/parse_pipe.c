/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_pipe.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/24 02:02:02 by ccano             #+#    #+#             */
/*   Updated: 2014/06/26 10:10:45 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh.h>

static int	expect(t_parser *s_parser)
{
	if (s_parser->s_lexem->type == S_ALPHA)
		return (parse_cmd(s_parser));
	parse_fatal(s_parser);
	return (ERR_PARSING);
}

int			parse_pipe(t_parser *s_parser)
{
	int		ret;

	ret = ERR_PARSING;
	if ((ft_strlen(s_parser->s_lexem->lexem) == 1) && (ret = 1))
		s_parser->s_ast->s_cmd->piped = 1;
	s_parser->s_lexem = s_parser->s_lexem->next;
	if ((ret > 0) && (s_parser->s_lexem))
		return (expect(s_parser));
	parse_fatal(s_parser);
	return (ERR_PARSING);
}
