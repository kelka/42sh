/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_ri.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/24 02:18:01 by ccano             #+#    #+#             */
/*   Updated: 2014/06/26 10:14:44 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh.h>

int			build_redi(t_parser *s_parser)
{
	if (!(s_parser->s_ast->s_cmd))
		build_cmd(s_parser);
	if (s_parser->s_ast->s_cmd->s_redi)
	{
		if (!(s_parser->s_ast->s_cmd->s_redi->next = \
					malloc(sizeof(*s_parser->s_ast->s_cmd->s_redi))))
			return (0);
		s_parser->s_ast->s_cmd->s_redi->next->prev = \
				s_parser->s_ast->s_cmd->s_redi;
		s_parser->s_ast->s_cmd->s_redi = s_parser->s_ast->s_cmd->s_redi->next;
	}
	else if (!(s_parser->s_ast->s_cmd->s_redi = \
			malloc(sizeof(*s_parser->s_ast->s_cmd->s_redi)))
			|| (s_parser->s_ast->s_cmd->s_redi
				&& (s_parser->s_ast->s_cmd->s_redi->prev = NULL)))
		return (0);
	s_parser->s_ast->s_cmd->s_redi->file = NULL;
	s_parser->s_ast->s_cmd->s_redi->next = NULL;
	return (1);
}

static int	expect_r(t_parser *s_parser)
{
	if (s_parser->s_lexem && (s_parser->s_lexem->type == S_ALPHA))
		return (parse_cmd(s_parser));
	parse_fatal(s_parser);
	return (ERR_PARSING);
}

int			parse_rir(t_parser *s_parser)
{
	if (s_parser->s_lexem && ft_strlen(s_parser->s_lexem->lexem) < 3)
	{
		if (!(build_redi(s_parser)))
			return (ERR_MALLOC);
		if (*(s_parser->s_lexem->lexem + 1) == '<')
			s_parser->s_ast->s_cmd->s_redi->append = 1;
		else
			s_parser->s_ast->s_cmd->s_redi->append = 0;
		s_parser->s_lexem = s_parser->s_lexem->next;
		if (s_parser->s_lexem)
		{
			s_parser->s_ast->s_cmd->s_redi->file = s_parser->s_lexem->lexem;
			s_parser->s_lexem = s_parser->s_lexem->next;
			return (expect_r(s_parser));
		}
	}
	parse_fatal(s_parser);
	return (ERR_PARSING);
}

static int	expect_l(t_parser *s_parser)
{
	if (!s_parser->s_lexem)
		return (parse_eof(s_parser));
	if (s_parser->s_lexem->type == S_SEP)
		return (parse_sep(s_parser));
	if (s_parser->s_lexem->type == S_PIPE)
		return (parse_pipe(s_parser));
	if (s_parser->s_lexem->type == S_REDI)
		return (parse_ril(s_parser));
	parse_fatal(s_parser);
	return (ERR_PARSING);
}

int			parse_ril(t_parser *s_parser)
{
	if (ft_strlen(s_parser->s_lexem->lexem) < 3)
	{
		if (!(build_redi(s_parser)))
			return (ERR_MALLOC);
		if (*(s_parser->s_lexem->lexem + 1) == '<')
			s_parser->s_ast->s_cmd->s_redi->append = 1;
		else
			s_parser->s_ast->s_cmd->s_redi->append = 0;
		s_parser->s_ast->s_cmd->s_redi->file = s_parser->s_lexem->next->lexem;
		s_parser->s_lexem = s_parser->s_lexem->next->next;
		return (expect_l(s_parser));
	}
	parse_fatal(s_parser);
	return (ERR_PARSING);
}
