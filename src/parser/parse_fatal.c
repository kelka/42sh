/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_fatal.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/24 02:33:56 by ccano             #+#    #+#             */
/*   Updated: 2014/06/24 03:25:15 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh.h>

void		parse_fatal(t_parser *s_parser)
{
	if (s_parser->s_lexem)
	{
		ft_putstr("Parse Error at ");
		ft_putstr(s_parser->s_lexem->lexem);
		write(1, "\n", 1);
	}
	else
		ft_putstr("Parse Error\n");
	free(s_parser);
}
