/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_init.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/24 02:38:38 by ccano             #+#    #+#             */
/*   Updated: 2014/06/26 09:52:41 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh.h>

static int	expect(t_parser *s_parser)
{
	if (!s_parser->s_lexem)
		return (parse_eof(s_parser));
	if (s_parser->s_lexem->type == S_ALPHA)
		return (parse_cmd(s_parser));
	else if (s_parser->s_lexem->type == S_REDI)
		return (parse_rir(s_parser));
	else if (s_parser->s_lexem->type == S_SEP)
		return (parse_sep(s_parser));
	parse_fatal(s_parser);
	return (ERR_PARSING);
}

int			parse_init(t_parser *s_parser)
{
	return (expect(s_parser));
}
