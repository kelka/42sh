/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_sep.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/24 02:28:53 by ccano             #+#    #+#             */
/*   Updated: 2014/06/24 02:31:40 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh.h>

int			parse_sep(t_parser *s_parser)
{
	while (s_parser->s_lexem && s_parser->s_lexem->type == S_SEP)
		s_parser->s_lexem = s_parser->s_lexem->next;
	return (1);
}
