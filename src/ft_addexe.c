/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_addexe.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scarre <scarre@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/27 22:09:49 by scarre            #+#    #+#             */
/*   Updated: 2014/03/27 22:11:58 by scarre           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh.h>

t_exe		*addexe(t_exe *mapexe, char *exe, int ac)
{
	t_exe	*tmp;
	size_t	cn;

	tmp = mapexe;
	cn = -1;
	while (exe[++cn])
	{
		if (!(tmp->next[((int)exe[cn])]))
		{
			tmp->next[((int)exe[cn])] = init_exe(tmp->next[((int)exe[cn])]);
			tmp = tmp->next[((int)exe[cn])];
		}
		else
			tmp = tmp->next[((int)exe[cn])];
	}
	if (exe[cn] == '\0')
	{
		tmp->ac = ac;
		if (tmp->pexe)
		{
			ft_bzero(tmp->pexe, ft_strlen(tmp->pexe));
			free(tmp->pexe);
		}
	}
	return (mapexe);
}
