/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lverniss <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/25 00:35:06 by lverniss          #+#    #+#             */
/*   Updated: 2014/06/26 01:30:05 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <sh.h>

static int		init_t_exe_fd(t_exe_fd **s_exe)
{  
	if (!(*s_exe) && !(*s_exe = (t_exe_fd *)malloc(sizeof(**s_exe))))
		return (ERR_MALLOC);
	(*s_exe)->in_bk = dup(fileno(stdin));
	(*s_exe)->out_bk = dup(fileno(stdout));
	(*s_exe)->fd_in = (*s_exe)->in_bk;
	(*s_exe)->fd_out = (*s_exe)->out_bk;
	return (0);
}

static int	tc_init(t_sh *s_sh)
{
	if ((tgetent(s_sh->user->term, NULL)) == ERR)
		return (ERR_TERM);
	s_sh->term_save->c_lflag &= ~(ICANON);
	s_sh->term_save->c_lflag &= ~(ECHO);
	s_sh->term_save->c_cc[VMIN] = 1;
	s_sh->term_save->c_cc[VTIME] = 0;
	if ((tcsetattr(STDIN, TCSANOW, s_sh->term_save) < 0))
		return (ERR_TERM);
	return (EXIT_SUCCESS);
}

/*
int				get_cmd(char *line, t_sh *sh)
{
	t_lexem		*s_lexem;
	t_parser		*s_parser;
	t_ast		*s_ast;
	static t_exe_fd		*s_exe = NULL;
	int			ret;

	ret = 3;
	if (((ret = sh_lexer(line, &s_lexem)) == EXIT_SUCCESS))
	{
		ret = sh_parser(s_lexem, &s_parser);
		if (ret > -1)
		{
			s_ast = s_parser->s_ast;
			while (s_ast)
			{
#if __DEBUG_EXEC__
				printf("next ast\n");
#endif
				if (init_t_exe_fd(&s_exe) == ERR_MALLOC)
					return (ERR_MALLOC);
				s_exe->job = new_job(sh);
				if (s_ast->bkg)
					ret = sh_exec_bkg(s_ast->s_cmd, s_exe, sh, 1);
				else
					ret = sh_exec_bkg(s_ast->s_cmd, s_exe, sh, 0);
				printf("exit status: %d\n", ret);
				s_ast = s_ast->next;
				ret = 1;
			}
		}
		else
			ret = 1;
	}
	return (ret);
}
*/
static int		init_shell(t_sh *s_sh)
{
	if (isatty(0))
	{
		if (!(s_sh->term_save = ft_memalloc(sizeof(*s_sh->term_save))))
			return (ERR_MALLOC);
		while (tcgetpgrp(fileno(stdin)) != getpgrp())
			kill (-s_sh->pgid, SIGTTIN);
		signal(SIGINT, SIG_IGN);
		signal(SIGQUIT, SIG_IGN);
		signal(SIGTSTP, SIG_IGN);
		signal(SIGTTIN, SIG_IGN);
		signal(SIGTTOU, SIG_IGN);
		signal(SIGCHLD, SIG_IGN);
		s_sh->pgid = getpid();
		if (setpgid(s_sh->pgid, s_sh->pgid) < 0)  
		{
			printf("Failed to set group id of the main process.\n");
			return (ERR_SETPGID);
		}
		tcsetpgrp(0, s_sh->pgid);
		tcgetattr(0, s_sh->term_save);
		if (tc_init(s_sh) != EXIT_SUCCESS)
			return (ERR_TERM);
		return (1);
	}
	return (0);
}

char	core(t_sh *sh)
{
	char	*new;
	int		ret;

	aff_prompt(sh);
	ret = -1;
	if (!(new = ft_readcmd())) /* le new ici c'est le retour du read en char * il faut pencer a le free*/
	{
		free(new);
		return (1);
	}
	if ((ret = get_cmd(new, sh)) == ERR_LEXING)
		ret = 1;
	free(new);
	return (ret);
}


t_sh	*loadshell(t_sh *sh, char **envi)
{
	if (!(sh = malloc(sizeof(*sh))))
		return (0);
	if (!(sh->env = ft_cpyenv(envi)))
		return (0);
	if (!(sh->user = malloc(sizeof(*sh->user))))
		return (0);
	sh->user->user = ft_getenv(sh->env, "USER=");
	sh->user->pwd = ft_getenv(sh->env, "PWD="); /* le chargement de l'environnement */
	sh->user->path = ft_getenv(sh->env, "PATH=");
	sh->user->term = ft_getenv(sh->env, "TERM=");
	sh->user->oldpwd = ft_getenv(sh->env, "OLDPWD=");
	sh->user->home = ft_getenv(sh->env, "HOME=");
	sh->mapexe = ft_mapexe(sh);
	sh = addbuiltin(sh);
	sh = pathuser(sh);
	ft_shrc(sh);
	sh->ae = 1;
	sh->exit = 0;
	return (sh);
}

/* 
 * IL FAUT PEUT ETRE RAJOUTER DES TRUC DANS LES STRUCTURES !
 */
static void		tc_reset(struct termios *term)
{
	term->c_lflag |= ~(ICANON);
	term->c_lflag |= ~(ECHO);
	tcsetattr(STDIN, TCSANOW, term);
}

int		main(int ac, char **av, char **envi)   
{
	t_sh	*sh;
	int		ret;

	if (ac && !av)
		return (0);
	sh = NULL;
	if (!(sh = loadshell(sh, envi)))
		return (0);
	if (!(ret = init_shell(sh)))
		return (0);
	else if (ret == ERR_TERM)
	{
		printf("Cannot load termcap infos.\n");
		return (0);
	}
	//	if (!(loadconfig(sh)))
	while (((core(sh)) > 0) && !(sh->exit))
		;
	tc_reset(sh->term_save);
	return (0);
}
