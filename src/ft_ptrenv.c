/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ptrenv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scarre <scarre@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/27 22:25:30 by scarre            #+#    #+#             */
/*   Updated: 2014/03/27 22:26:14 by scarre           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh.h>

t_env		*r_env_ptr(t_env *env, char *toseek)
{
	t_env	*tmp;

	tmp = env;
	while (tmp)
	{
		if (ft_strncmp(tmp->line, toseek, ft_strlen(toseek)))
			return (tmp);
		tmp = tmp->next;
	}
	return (NULL);
}
