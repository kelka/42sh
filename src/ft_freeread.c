/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_freeread.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scarre <scarre@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/27 22:13:24 by scarre            #+#    #+#             */
/*   Updated: 2014/03/27 22:13:25 by scarre           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh.h>

void	ft_freeread(t_read *s)
{
	t_read	*tmp;

	while (s)
	{
		tmp = s;
		ft_bzero(tmp->buff, tmp->len);
		s = s->next;
		free(tmp);
	}
	free(s);
}
