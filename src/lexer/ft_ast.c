/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ast.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/18 16:30:42 by ccano             #+#    #+#             */
/*   Updated: 2014/03/27 22:20:10 by scarre           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh_parser.h>

static t_ast		*ast_new(void)
{
	t_ast			*s_ast;

	s_ast = NULL;
	s_ast = (t_ast *)ft_memalloc(sizeof(*s_ast));
	return (s_ast);
}

t_ast				*ast_new_pushback(t_ast **s_bast)
{
	t_ast			*s_ast;

	s_ast = NULL;
	if (s_bast && !(*s_bast))
	{
		*s_bast = ast_new();
		s_ast = *s_bast;
	}
	else if (s_bast && *s_bast)
	{
		s_ast = *s_bast;
		while (s_ast->next)
			s_ast = s_ast->next;
		s_ast->next = ast_new();
		s_ast->next->prev = s_ast;
		s_ast = s_ast->next;
	}
	return (s_ast);
}
