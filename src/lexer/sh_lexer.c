/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sh_lexer.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/18 16:33:11 by ccano             #+#    #+#             */
/*   Updated: 2014/06/26 11:38:01 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh.h>

static int			init_states(char *states)
{
	int				n;

	n = -1;
	while (++n < 127)
	{
		if (n == '<')
			states[n] = S_REDI;
		else if (n == '>')
			states[n] = S_REDO;
		else if (n == '|')
			states[n] = S_PIPE;
		else if ((n == ' ') || (n == '\t'))
			states[n] = S_SPACE;
		else if (n == ';')
			states[n] = S_SEP;
		else if (n == '&')
			states[n] = S_BKG;
		else if (n == '\0')
			states[n] = S_END;
		else if (n >= '!' && n <= '~')
			states[n] = S_ALPHA;
		else
			states[n] = S_ELSE;
	}
	return (1);
}

static int			sh_mtx(char *line, t_lexem **s_blexem)
{
	char			*states;
	int				ret;
	char			**mtx_trans;

	if (!(mtx_trans = (char **)ft_strnew(sizeof(**mtx_trans) * NB_STATE)))
		return (ERR_MALLOC);
	if (!(states = ft_strnew(sizeof(*states) * 127)))
		return (-1);
	if (!(init_states(states)))
		return (-2);
	ret = fsm_lex(states, line, s_blexem);
	free(states);
	return (ret);
}

int					sh_lexer(char *line, t_lexem **s_rlexem)
{
	int				ret;
	t_lexem			*s_blexem;
	t_lexem			*s_plexem;

	ret = -2;
	if (line && *line)
		ret = sh_mtx(line, &s_blexem);
	if (ret >= 0)
		del_s_space(&s_blexem);
	s_plexem = s_blexem;
#if __DEBUG_LEXER__
	while (s_plexem)
	{
		printf("lexem: %s\ttype: %d\n", s_plexem->lexem, s_plexem->type);
		s_plexem = s_plexem->next;
	}
#endif
	*s_rlexem = s_blexem;
	return (ret);
}
