/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fsm_ft.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccano <ccano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/18 16:20:46 by ccano             #+#    #+#             */
/*   Updated: 2014/06/26 11:38:33 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fsm.h>
#include <sh.h>
#include <mtx_trans.h>

int			fsm_bof(t_fsm *s_fsm)
{
	int		next;

	s_fsm->b_lexem = s_fsm->current;
	s_fsm->old_state = s_fsm->state;
	++s_fsm->current;
	s_fsm->state = s_fsm->states[(int)(*s_fsm->current)];
	next = g_mtx_state[s_fsm->old_state][s_fsm->state];
	return (s_fsm->ft_fsm[next](s_fsm));
}

int			fsm_eof(t_fsm *s_fsm)
{
	size_t	lexem_len;

	lexem_len = s_fsm->current - s_fsm->b_lexem;
	if (lexem_len == 0)
		++lexem_len;
	if ((new_lexem_pushback(s_fsm)) < 0)
		return (-1);
	if (!(s_fsm->s_lexem->lexem = ft_strnew(lexem_len + 1)))
		return (-1);
	s_fsm->s_lexem->lexem = ft_memcpy(s_fsm->s_lexem->lexem, s_fsm->b_lexem,
			lexem_len);
	s_fsm->s_lexem->type = s_fsm->old_state;
	return (0);
}

int			fsm_new_lex(t_fsm *s_fsm)
{
	size_t	lexem_len;
	int		next;

	lexem_len = s_fsm->current - s_fsm->b_lexem;
	if (lexem_len < 1)
		lexem_len = 1;
	if ((new_lexem_pushback(s_fsm)) < 0)
		return (-1);
	if (!(s_fsm->s_lexem->lexem = ft_strnew(lexem_len + 1)))
		return (-1);
	s_fsm->s_lexem->lexem = (char *)ft_memcpy(s_fsm->s_lexem->lexem,
			s_fsm->b_lexem, lexem_len);
	s_fsm->s_lexem->type = s_fsm->old_state;
	s_fsm->b_lexem = s_fsm->current;
	s_fsm->old_state = s_fsm->state;
	s_fsm->state = s_fsm->states[(int)(*s_fsm->current)];
	next = g_mtx_state[s_fsm->old_state][s_fsm->state];
	return (s_fsm->ft_fsm[next](s_fsm));
}

int			fsm_inside_lex(t_fsm *s_fsm)
{
	int		next;

	++s_fsm->current;
	s_fsm->old_state = s_fsm->state;
	s_fsm->state = s_fsm->states[(int)(*s_fsm->current)];
	next = g_mtx_state[s_fsm->old_state][s_fsm->state];
	return (s_fsm->ft_fsm[next](s_fsm));
}

int			fsm_error(t_fsm *s_fsm)
{
	ft_putstr_fd("Lexing error near unexpected ", 2);
	write(2, s_fsm->current, 1);
	ft_putchar('\n');
	free(s_fsm->ft_fsm);
	free(s_fsm);
	return (ERR_LEXING);
}
