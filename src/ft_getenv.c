/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getenv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scarre <scarre@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/27 22:20:30 by scarre            #+#    #+#             */
/*   Updated: 2014/03/27 22:20:49 by scarre           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh.h>

char		*ft_getenv(t_env *env, char *toseek)
{
	t_env	*tmp;
	int		i;

	i = ft_strlen(toseek);
	tmp = env;
	while (tmp && tmp->next)
	{
		if ((ft_strncmp(tmp->line, toseek, i)))
			return (&tmp->line[i]);
		tmp = tmp->next;
	}
	return (0);
}
