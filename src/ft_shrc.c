/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_shrc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scarre <scarre@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/27 22:31:18 by scarre            #+#    #+#             */
/*   Updated: 2014/03/27 22:35:14 by scarre           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# include <sh.h>

char		create_filerc(t_sh *sh)
{
	int		fd;
	const char	*str = "#   42SHRC FILECONF\
				\n# For help use commande help-42shrc\
				\n#Color inclued : BLACK BLACKBOLD\
				\n#RED REDBOLD GREEN GREENBOLD YELLOW YELLOWBOLD\
				\n#BLUE BLUEBOLD PURPLE PURPLEBOLD CYAN CYANBOLD WHITE WHITEBOLD\
				\nprompt-user = 0\
				\nprompt-color = redbold\
				\nprompt-locate = 0\
				\nprompt-pwd = 0\
				\ngod-mod = off";

	if (!(fd = creat(sh->user->pshrc, S_IRWXU)))
		ft_error("fail to create .42shrc.\n");
	write(fd, str, ft_strlen(str));
	close(fd);
	return (1);
}

char		*reverse_file(t_read *s)
{
	t_read	*tmp;
	char	*new;
	size_t	cn[2] = {0, -1};

	tmp = s;
	while (tmp)
	{
		cn[0] += tmp->len;
		tmp = tmp->next;
	}
	if (!(new = malloc(sizeof(*new) * (cn[0] + 1))))
		return (0);
	tmp = s;
	while (tmp)
	{
		cn[0] = -1;
		while (++cn[0] < tmp->len)
			new[++cn[1]] = tmp->buff[cn[0]];
		tmp = tmp->next;
	}
	new[++cn[1]] = '\0';
	return (new);
}

char		*whitespace(char *str)
{
	size_t	cn[2] = {-1, 0};
	char	*new;

	cn[0] = -1;
	while (str[++cn[0]])
	{
		if (str[cn[0]] == ' ' && str[cn[0]] == '\t')
			++cn[1];
	}
	if (!(new = malloc(sizeof(*new) * ((ft_strlen(str) - cn[1]) + 1))))
		return (0);
	cn[0] = -1;
	cn[1] = -1;
	while (str[++cn[0]])
	{
		if (str[cn[0]] != ' ' && str[cn[0]] != '\t')
			new[++cn[1]] = str[cn[0]];
	}
	new[cn[1]] = '\0';
	ft_bzero(str, ft_strlen(str));
	free(str);
	return (new);
}

void		free_f(char **lol)
{
	size_t	cn;

	cn = -1;
	while (lol[++cn])
	{
		ft_bzero(lol[cn], ft_strlen(lol[cn]));
		free(lol[cn]);
	}
	free(lol);
}

t_rc		*prompt_user(char *str, t_rc *rc)
{
	if (samestr("1", str))
		rc->uac = 1;
	else if (samestr("0", str))
		rc->uac = 0;
	else
	{
		ft_error(".42shrc prompt-user error default mode.\n");
		rc->uac = 1;
	}
	return (rc);	
}

t_rc		*prompt_color(char *str, t_rc *rc)
{
	if (samestr("red", str) || samestr("RED", str))
		rc->color = 1;
	else if (samestr("blue", str) || samestr("WHITE", str))
		rc->color = 2;
	else if (samestr("white", str) || samestr("WHITE", str))
		rc->color = 3;
	else if (samestr("0", str) || samestr("none", str) || samestr("NONE", str))
		rc->color = 0;
	else if (samestr("yellow", str) || samestr("YELLOW", str))
		rc->color = 4;
	else if (samestr("black", str) || samestr("BLACK", str))
		rc->color = 5;
	else if (samestr("blackbold", str) || samestr("BLACKBOLD", str))
		rc->color = 6;
	else if (samestr("redbold", str) || samestr("REDBOLD", str))
		rc->color = 7;
	else if (samestr("green", str) || samestr("GREEN", str))
		rc->color = 8;
	else if (samestr("greenbold", str) || samestr("GREENBOLD", str))
		rc->color = 9;
	else if (samestr("yellowbold", str) || samestr("YELLOWBOLD", str))
		rc->color = 10;
	else if (samestr("bluebold", str) || samestr("BLUEBOLD", str))
		rc->color = 11;
	else if (samestr("purple", str) || samestr("PURPLE", str))
		rc->color = 12;
	else if (samestr("purplebold", str) || samestr("PURPLEBOLD", str))
		rc->color = 13;
	else if (samestr("cyan", str) || samestr("CYAN", str))
		rc->color = 14;
	else if (samestr("cyanbold", str) || samestr("CYANBOLD", str))
		rc->color = 15;
	else if (samestr("whitebold", str) || samestr("WHITEBOLD", str))
		rc->color = 16;
	else
	{
		ft_error(".42shrc prompt-color error default mode.\n");
		rc->color = 3;
	}
	return (rc);
}

t_rc		*prompt_locate(char *str, t_rc *rc)
{
	if (samestr("1", str))
		rc->lac = 1;
	else if (samestr("0", str))
		rc->lac = 0;
	else
	{	
		ft_error(".42shrc prompt-locate error default mode.\n");
		rc->lac = 0;
	}
	return (rc);
}

t_rc		*prompt_pwd(char *str, t_rc *rc)
{
	if (samestr("1", str))
		rc->pac = 1;
	else if (samestr("0", str))
		rc->pac = 0;
	else
	{
		ft_error(".42shrc prompt-pwd error default mode.\n");
		rc->pac = 1;
	}
	return (rc);
}

ptrc		*create_ptrfonc(void)
{
	ptrc	*re;

	if (!(re = malloc(sizeof(*re) * (4))))
		return (0);
	re[0] = prompt_user;
	re[1] = prompt_color;
	re[2] = prompt_locate;
	re[3] = prompt_pwd;
	return (re);
}

t_rc		*fullrc(char *str, size_t si, t_rc *rc)
{
	ptrc	*prompt;

	if (!(prompt = create_ptrfonc()))
		return (0);
	rc = prompt[si](str, rc);
	return (rc);
}

t_rc		*loadinfo(t_split *s, t_rc *rc)
{
	size_t	cn;
	size_t	a;
	const char	*str[5] = {"prompt-user", 
			"prompt-color", 
			"prompt-locate", 
			"prompt-pwd", 
			NULL};

	while (s && s->next)
	{
		cn = -1;
		while (s->tmp[++cn])
		{
			a = -1;
			while (str[++a])
			{
				if (ft_strncmp((char *)str[a], (char *)s->tmp[cn], ft_strlen(s->tmp[cn])))
					rc = fullrc(s->tmp[cn + 1], a, rc);
			}
		}
		s = s->next;
	}
	return (rc);
}

void		freetsplit(t_split *s)
{
	size_t	cn;
	t_split	*tmp;

	while (s && s->next)
	{
		tmp = s;
		cn = -1;
		while (tmp->tmp[++cn])
		{
			ft_bzero(tmp->tmp[cn], ft_strlen(tmp->tmp[cn]));
			free(tmp->tmp[cn]);
		}
		free(tmp->tmp);
		s = s->next;
		ft_bzero(tmp, sizeof(*tmp));
		free(tmp);
	}
	free(s);
}

void		configdatauser(t_rc *rc, char *data)
{
	char	**tmp;
	size_t	cn;
	char	*new;
	t_split	*s;
	t_split	*opt;

	if (!(opt = malloc(sizeof((*opt)))))
		return ;
	s = opt;
	cn = -1;
	new = whitespace(data);
	tmp = ft_strsplit(new, '\n');
	while (tmp[++cn])
	{
		if (tmp[cn][0] != '\0' && tmp[cn][0] != '#')
		{
			opt->tmp = ft_strsplit(tmp[cn], '=');
			if (!(opt->next = malloc(sizeof(*opt))))
				return ;
			opt = opt->next;
		}	
	}
	opt->next = NULL;
	free_f(tmp);
	loadinfo(s, rc);
	freetsplit(s);
}

char		parse_filerc(t_sh *sh)
{
	t_read	*s;
	t_read	*tmp;
	int		fd;
	char	*new;

	if (!(fd = open(sh->user->pshrc, O_RDONLY)))
		return (0);
	if (!(tmp = malloc(sizeof(*tmp))))
		return (0);
	s = tmp;
	while ((tmp->len = read(fd, tmp->buff, BUFF_SIZE)) == BUFF_SIZE)
	{
		if (!(tmp->next = malloc(sizeof(*tmp))))
			return (0);
		tmp = tmp->next;
	}
	tmp->next = NULL;
	new = reverse_file(s);
	ft_freeread(s);
	configdatauser(&sh->user->rc, new);
	return (1);
}

char		ft_shrc(t_sh *sh)
{
	if ((access(sh->user->pshrc, F_OK)) == -1)
	{
		ft_error(".42shrc not found !\n");
		create_filerc(sh);
		ft_error(".42shrc is now generated in your home.\n");
	}
	parse_filerc(sh);
	return (1);
}
