/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_pexe.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scarre <scarre@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/27 23:33:06 by scarre            #+#    #+#             */
/*   Updated: 2014/06/26 05:53:23 by ccano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh.h>

int				get_pexe(t_sh *sh, t_cmd *cmd)
{
   t_exe		*tmp;
   int			ret;
   char			*file;

   tmp = sh->mapexe;
   file = cmd->argv[0];
   ret = 0;
   while (*file && tmp)
   {
	  tmp = tmp->next[(int)(*file)];
	  ++file;
   }
   if (tmp && (tmp->pexe || (tmp->ac > 1)) && (ret = 1))
   {
	  cmd->perm = tmp->ac;
	  cmd->filename = tmp->pexe;
   }
   return (ret);
}

int				get_filename(t_cmd *s_cmd, t_sh *sh)
{
	if (s_cmd->argv[0] && (*s_cmd->argv[0] == '/'))   
	{
		s_cmd->filename = s_cmd->argv[0];
		return (1);
	}
	else if (s_cmd->argv[0] && (*s_cmd->argv[0] == '.')
			&& (*(s_cmd->argv[0] + 1) == '/'))
	{
		s_cmd->filename = s_cmd->argv[0] + 2;
		return (1);
	}
	else if ((get_pexe(sh, s_cmd)))
		return (1);
	return (0);
}


