/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_addbuiltin.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scarre <scarre@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/27 22:09:27 by scarre            #+#    #+#             */
/*   Updated: 2014/03/27 22:19:33 by scarre           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh.h>

t_sh	*addbuiltin(t_sh *sh)
{
	sh->mapexe = addexe(sh->mapexe, "cd", 2);
	sh->mapexe = addexe(sh->mapexe, "env", 3);
	sh->mapexe = addexe(sh->mapexe, "exit", 4);
	return (sh);
}
