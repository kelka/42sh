/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_prompt.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scarre <scarre@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/27 22:15:57 by scarre            #+#    #+#             */
/*   Updated: 2014/03/27 23:28:54 by scarre           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh.h>

void			acolor(int i)
{
	const char	*color[16] = {"\033[1;31m", 
							"\033[0;34m",
							"\033[0;37m",
							"\033[0;33m",
							"\033[0;30m",
							"\033[1;30m",
							"\033[1;31m",
							"\033[0;32m",
							"\033[1;32m",
							"\033[1;33m",
							"\033[1;34m",
							"\033[0;35m",
							"\033[1;35m",
							"\033[0;36m",
							"\033[1;36m",
							"\033[1;37m"};

	i = i - 1;
	if (i > 0)
		write(1, color[i], ft_strlen(color[i]));
}

void			aff_user(int ac, t_sh *sh)
{
	if (ac == 1)
		write(1, sh->user->user, ft_strlen(sh->user->user));
}

void			aff_pwd(int ac, t_sh *sh)
{
	size_t		cn;

	sh->user->pwd = ft_getenv(sh->env, "PWD=");
	cn = 0;
	if (ac == 1)
	{
		while (sh->user->pwd[cn] == sh->user->home[cn])
			cn++;
		write(1, ":", 1);
		if (sh->user->home[cn + 1] == '\0')
		{
			write(1, "~", 1);
			if (!(samestr(sh->user->pwd, sh->user->home)))
				write(1, &sh->user->pwd[cn], ft_strlen(&sh->user->pwd[cn]));
			else
				write(1, "/", 1);
		}
		else
			write(1, sh->user->pwd, ft_strlen(sh->user->pwd));
	}
}

void			aff_locate(int ac)
{
	if (ac == 1)
	{
		write(1, "@", 1);
		write(1, "BeHuman", 7);
	}
}

int				aff_prompt(t_sh *sh)
{
	const char	*end = "\033[0;00m";
	acolor(sh->user->rc.color);
	aff_user(sh->user->rc.uac, sh);
	aff_locate(sh->user->rc.lac);
	aff_pwd(sh->user->rc.pac, sh);
	write(1, " :>", 3);
	if (sh->user->rc.color > 0)
		write(1, end, ft_strlen(end));
	return (1);
}
