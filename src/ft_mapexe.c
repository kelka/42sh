/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mapexe.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lverniss <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/28 01:47:10 by lverniss          #+#    #+#             */
/*   Updated: 2014/03/27 22:15:17 by scarre           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh.h>

char	*create_src(char *dir, char *name)
{
	size_t	cn[2] = {-1, -1};
	char	*new;

	if (name == NULL)
		return (0);
	if (dir == NULL)
		return (0);
	if (!(new = malloc(sizeof(*new) * (ft_strlen(dir) + ft_strlen(name) + 2))))
		return (0);
	while (dir[++cn[0]])
		new[cn[0]] = dir[cn[0]];
	new[cn[0]] = '/';
	while (name[++cn[1]])
		new[++cn[0]] = name[cn[1]];
	new[++cn[0]] = '\0';
	return (new);
}

t_exe	*ft_mapexe(t_sh *sh)
{
	DIR				*dir;
	struct dirent	*name;
	size_t			cn[3] = {-1, -1, -1};
	char			**path;
	char			*ntmp;
	t_exe			*s;
	t_exe			*tmp;

	s = NULL;
	s = init_exe(s);
	tmp = s;
	path = ft_strsplit(sh->user->path, ':');
	while (path[++cn[0]])
	{
		if (!(dir = opendir(path[cn[0]])))
			continue;
		while ((name = readdir(dir)))
		{
			cn[1] = -1;
			while (name->d_name[++cn[1]])
			{
				if ((tmp->next[((int)name->d_name[cn[1]])]) == NULL)
				{
					if (!(tmp->next[((int)name->d_name[cn[1]])] = init_exe(tmp->next[((int)name->d_name[cn[1]])])))
						return (0);
					tmp = tmp->next[((int)name->d_name[cn[1]])];
				}
				else
					tmp = tmp->next[((int)name->d_name[cn[1]])];
			}
			if (name->d_name[cn[1]] == '\0')
			{
				if (!(tmp->pexe = create_src(path[cn[0]], name->d_name)))
					tmp = s;
				ntmp = ft_strdup(tmp->pexe);
				tmp = s;
				cn[2] = -1;
				while (ntmp[++cn[2]])
				{
					if (!(tmp->next[((int)ntmp[cn[2]])]))
					{
						if (!(tmp->next[((int)ntmp[cn[2]])] = init_exe(tmp->next[((int)ntmp[cn[2]])])))
							return (0);
						tmp = tmp->next[((int)ntmp[cn[2]])];
					}
					else
						tmp = tmp->next[((int)ntmp[cn[2]])];
				}
				if (ntmp[cn[2]] == '\0')
				{
					tmp->pexe = ft_strdup(ntmp);
					ft_bzero(ntmp, ft_strlen(ntmp));
					free(ntmp);
				}
				cn[1] = -1;
			}
			tmp = s;
		}
		free(path[cn[0]]);
		closedir(dir);
	}
	free(path);
	return (s);
}
