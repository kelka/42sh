/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scarre <scarre@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/27 22:26:38 by scarre            #+#    #+#             */
/*   Updated: 2014/03/27 22:29:04 by scarre           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sh.h>
#include <string.h>

static char		test_cmdline(char *str)
{
	size_t		cn;

	cn = -1;
	while (str[++cn])
	{
		if (str[cn] != ' ' && str[cn] != '\t')
			return (1);
	}
	return (0);
}

static char		*reverse_read(t_char *start)
{
	char		*new;
	t_char		*tmp;
	size_t		cn;

	tmp = start;
	cn = 0;
	while (tmp)
	{
		cn++;
		tmp = tmp->next;
	}
	tmp = start;
	if (!(new = malloc(sizeof(*new) * cn + 1)))
		return (0);
	cn = 0;
	while (tmp)
	{
		new[cn] = tmp->c;
		tmp = tmp->next;
		cn++;
	}
	new[cn - 1] = '\0';
	if (!(test_cmdline(new)))
		return (0);
	return (new);
}

int				tcput(int c)
{
	char		tmp;

	tmp = c;
	write(1, &tmp, 1);
	return (1);
}


static int 		is_edit(char *buff, t_char **start, int *pos, t_char **current)
{
	t_char		*tmp;

	if (*buff == 127)
	{
		tmp = *current;
		if (*pos > 0)
		{
			tputs(tgetstr("le", NULL), 1, &tcput);
			tputs(tgetstr("ce", NULL), 1, &tcput);
			--*pos;
		}
		if (tmp->prev)
			tmp = tmp->prev;
		free(tmp->next);
		tmp->c = '\0';
		tmp->next = NULL;
		*current = tmp;
		return (1);
	}
	return (0);
}

static t_char	*read2(void)
{
	t_char		*s;
	t_char		*tmp2;
	int			pos;
	int			len;
	char		buff[1];

	if (!(tmp2 = malloc(sizeof(*tmp2))))
		return (0);
	tmp2->prev = NULL;
	tmp2->next = NULL;
	s = tmp2;
	ft_bzero(buff, 5);
	pos = 0;
	len = 0;
	while (((len = read(0, buff, 3)) > 0) && (*buff != '\n'))
	{
		if (*buff == '\n')
			exit(-1);
		else if (is_edit(buff, &s, &pos, &tmp2))
			;
		else if (len == 1)
		{
			tmp2->c = *buff;
			pos++;
			write(1, &tmp2->c, 1);
			if (!(tmp2->next = malloc(sizeof(*tmp2))))
				return (0);
			tmp2->next->prev = tmp2;
			tmp2 = tmp2->next;
			tmp2->next = NULL;
		}
		ft_bzero(buff, 1);
	}
	write(1, "\n", 1);
	tmp2->next = NULL;
	return (s);
}

char			*ft_readcmd(void)
{
	t_char		*start;
	t_char		*tmp;
	char		*new;

	if (!(start = read2()))
		return (0);
	if (!(new = reverse_read(start)))
		return (0);
	while (start)
	{
		tmp = start;
		start = start->next;
		free(tmp);
	}
	free(start);
	return (new);
}
